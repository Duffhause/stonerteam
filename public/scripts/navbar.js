var n = document.getElementById("navbarlinks");

const links = {
    "TCS Labs" : "https://www.thecryptosimps.com/",
    "UnfuckDonutTeam" : "https://unfuckdonutteam.roguecrab.com/"
}

for (const key in links) {
    var name = document.createTextNode(key);

    var btn = document.createElement("a");
    btn.innerHTML = key;
    btn.href=links[key];

    n.appendChild(btn);
    n.appendChild(document.createTextNode(" | "))
}

function RemoveJSPrompts() {
    var paras = document.getElementsByClassName("enablejsplease")
    document.querySelectorAll('.enablejsplease').forEach(e => e.remove());
}
