const info = {
    "alln" : {
        "Info" : null,
        "Release Date" : "22nd August 2021",
        "Last Update" : "7th September 2021",
        "Current Verison" : "1.02",
        "Category" : "Total Conversion Mods",
        "Language" : "English",
        "File" : null,
        "Filesize" : "208 MB",
        "Encrypted" : "No"
    },
    "crazytaxi" : {
        "Info" : null,
        "Release Date" : "11th November 2020",
        "Last Update" : "16th November 2020",
        "Current Verison" : "1.1.1",
        "Category" : "Total Conversion Mods",
        "Language" : "English",
        "File" : null,
        "Filesize" : "53 MB",
        "Encrypted" : "No"
    },
    "picklemod" : {
        "Info" : null,
        "Release Date" : "24th September 2020",
        "Last Update" : "20th April 2021",
        "Current Verison" : "1.1.4 / 1.1.4-L",
        "Category" : "Total Conversion Mods",
        "Language" : "English",
        "File" : null,
        "Filesize" : "Full Version - 1GB. Lite Version - 522MB",
        "Encrypted" : "No"
    },
    "acecashier" : {
        "Info" : null,
        "Release Date" : "27th December 2020",
        "Last Update" : "8th May 2021",
        "Current Verison" : "1.2",
        "Category" : "Total Conversion Mods",
        "Supported Langauges" : "English, Russian",
        "File" : null,
        "Filesize" : "",
        "Encrypted" : "No"
    },
    "lspaxplorer" : {
        "Info" : null,
        "Release Date" : "5th December 2021",
        "Last Update" : "5th December 2021",
        "Current Verison" : "1.0.1",
        "Source Code" : '<a target="_blank" href="https://github.com/duffhause/LSPAXplorer">Github</a>'
    },
    "crazytaxireturned" : {
        "Info" : null,
        "Release Date" : "28th December 2021",
        "Last Update" : "28th December 2021",
        "Current Verison" : "1.0",
        "Category" : "Add-ons",
        "Language" : "English",
        "File" : null,
        "Filesize" : "Full Version - 767 MB. Lite Version - 559MB",
        "Encrypted" : "No"
    },
    "mtv" : {
        "Info" : null,
        "Release Date" : "24th July 2019",
        "Last Update" : "28th July 2019",
        "Current Verison" : "1.1",
        "Category" : "Total Conversion Mods",
        "Language" : "English",
        "File" : null,
        "Filesize" : "37 MB",
        "Encrypted" : "No"
        }

}

function addData (mod){
    data = info[mod]
    var tbody = document.getElementById("infoboxbody");

    var odd = true;
    for (const key in data){
        // Insert a row at the end of table
        var newRow = tbody.insertRow();
        var nl = data[key] == null;

        if (!nl){
            var qcell = newRow.insertCell();
            var qcellp = document.createElement('p');
            var qcellb = document.createElement("b");
            qcellb.innerHTML = key;
            qcellp.appendChild(qcellb);
            qcell.appendChild(qcellp);


            var acell = newRow.insertCell();
            var acellp = document.createElement('p');
            acellp.innerHTML = data[key]
            acell.appendChild(acellp);

            if (odd)
            {
                //acell.style.backgroundColor = "#ededed";
                //qcell.style.backgroundColor = "#ededed";
            }
        }
        else {
            var cell = newRow.insertCell();
            var center = document.createElement('center');
            var text = document.createElement("p");
            text.innerHTML = key
            center.appendChild(text);
            cell.appendChild(center);
            cell.colSpan = "2";
            cell.style.backgroundColor = "lavender";
            odd = false;
        }
        odd = !odd
    }

}
