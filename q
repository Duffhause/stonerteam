[33mcommit 99574d23ae17f05e6cffa24737175e1ec5089112[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m)[m
Author: soul <soulstevens@outlook.com>
Date:   Mon Oct 4 15:33:42 2021 +0100

    responsive docs

[1mdiff --git a/public/css/docs.css b/public/css/docs.css[m
[1mindex a8822fe..75e6e21 100755[m
[1m--- a/public/css/docs.css[m
[1m+++ b/public/css/docs.css[m
[36m@@ -19,8 +19,15 @@[m [mbody {[m
 [m
     margin-right: 0px;[m
     margin-left: 170px;[m
[31m-    max-width: -moz-available;[m
[31m-    width:100%;[m
[32m+[m[32m    max-width: 1920px;[m
[32m+[m
[32m+[m[32m    width: auto;[m
[32m+[m[32m}[m
[32m+[m
[32m+[m[32m@media only screen and (max-width:800px) {[m
[32m+[m[32m        .docs{[m
[32m+[m[32m            margin-left: 0px;[m
[32m+[m[32m        }[m
 }[m
 [m
 .infobox {[m
[36m@@ -38,6 +45,14 @@[m [mbody {[m
      border: 0;[m
 }[m
 [m
[32m+[m[32m@media only screen and (max-width:800px) {[m
[32m+[m[32m        .infobox{[m
[32m+[m[32m            float: none;[m
[32m+[m[32m            width: auto;[m
[32m+[m[32m        }[m
[32m+[m[32m}[m
[32m+[m
[32m+[m
 .infobox img{[m
     width:100%;[m
 }[m
[1mdiff --git a/public/css/main.css b/public/css/main.css[m
[1mindex 405509b..698e723 100755[m
[1m--- a/public/css/main.css[m
[1m+++ b/public/css/main.css[m
[36m@@ -46,14 +46,20 @@[m [mtext-decoration: underline;[m
     margin-right: auto;[m
 	margin-top: 0px;[m
     height: 100%;[m
[31m-	width: 75%;[m
 	padding: 10px;[m
     /*background: #555;*/[m
 	background-image: linear-gradient(to left, #267F00, #555);[m
 	font-size: 170%;[m
 	color:#FFFFFF;[m
 	text-shadow: 1px 1px #267F00;[m
[31m-  max-width: 1000px;[m
[32m+[m[32m    max-width: 1000px;[m
[32m+[m[32m}[m
[32m+[m
[32m+[m[32m@media only screen and (max-width:800px) {[m
[32m+[m[32m    .content {[m
[32m+[m[32m        margin-left: 0px;[m
[32m+[m[32m        margin-right: 0px;[m
[32m+[m[32m    }[m
 }[m
 [m
 .content hr {[m
